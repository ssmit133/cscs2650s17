#include "functions.h"		//for gates
#include <stdio.h> 		//for i/o
#include <stdlib.h> 		//for exit
#include <string.h> 		//for string functions
/*
2:4 decoder

expected~
i1 i0 | o3 o2 o1 o0
--------------------
0   0 | 0   0  0  1
0   1 | 0   0  1  0
1   0 | 0   1  0  0
1   1 | 1   0  0  0 
*/
int main(int argc, char *argv[]){

//use literal p and o to compare against value 1 2 againist other variables
//compare the &a to o and p  to find equailvacy 

//declarations
char *x		= argv[1]; 	//i1
char *y		= argv[2]; 	//i0
char *c;			//o3
char *d; 			//o2
char *e; 			//o1
char *f; 			//o0

//input amount check
if(argc > 3){
    fprintf(stderr, "\nWrong amount of input, Try again!\n");
    exit(EXIT_FAILURE);
}

else{
	char* c = (char*)malloc(sizeof(char)); //o3
	*c = '0'; //give value
	c = and(x,y);

	char* d = (char*)malloc(sizeof(char)); //o2
	*d = '0';
	d = and(x,(not(y)));

	char* e = (char*)malloc(sizeof(char)); //o1
	*e = '0';
	e = and((not(x)),y);

	char* f = (char*)malloc(sizeof(char)); //o0
	*f = '0';
	f = and((not(x)),(not(y)));

	fprintf(stdout, "\nA B | C D E F\n");
	fprintf(stdout, "-------------\n");
	fprintf(stdout, "%s %s | %s %s %s %s\n", x,y,c,d,e,f);
    }

}

//////////////////////////////
//Im having the worst time with this latch
//

#include "functions.h"
#include <stdio.h>
#include <stdlib.h> //for atoi
#include <string.h>

int main(int argc, char ** argv){

char * 	s 	= 	argv[1];
char * 	r 	= 	argv[2];
char   	q 	= 	0;
char   	Nq 	= 	0;
char 	state	=	0;

if (argc > 3) {
    fprintf(stdout,"\nWrong amount of inputs!");
    exit(EXIT_FAILURE);
    }

if(s == 0 && r == 0) //checking for invalid mode CANT have 0x0 going in
    
    fprintf(stderr, "INVALID! Must have atleast a single 1 inputted");
    
    else {

	    if (s == 1) 
	    {
		q = nand(r,s);
		Nq = not(q);
		state = q;
	    }
	    else if (r == 1)
	    {
		q = nand(r,s);
		Nq = not(q);
		state = q;
	    }

	    fprintf(stdout, "\tA  B | C  D\n");
	    fprintf(stdout, "\t===========\n");
	    fprintf(stdout, "\t%s  %s | %s  %s\n",s,r,q,Nq);
	}

return(0);
}

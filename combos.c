#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//program to list all possible permutations of given numbers
//google: "permutations of a single bit transition numbers c program"
// a ? b : c
//variable = condition ? value_if_true : value_if_false
//evulates to b if the value of a is true or otherwise to c
//use turnary and arrays?

int main(int argc, char *argv[])
{
    FILE 	*fp = fopen ( "permutations.c", "a");
    char 	*choice;
    int 	num[10];
    int 	count = 0;
    
        
    //proper arguments please
    if (argc !=  2)
    {
	fprintf(stderr, "\nNope->\n\nPlease enter 1 string of DIGITS\n");
	exit(1);
    }
    
    //check file status
    if(fp == NULL)
    {
	fprintf(stdout, "\nError in Opening File!");
	exit(0);
    }
    
    while (*argv[1] != '\0')
    {
	choice = strtol(argv[1]);	 
	fprintf(fp, "\t%s\n", choice);
    }
    
    fclose(fp);
    return(0);
}

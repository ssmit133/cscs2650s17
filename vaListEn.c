#include "functions.h"		//for functions
#include <stdio.h> 		//for i/o
#include <stdlib.h> 		//for exit
#include <string.h> 		//for string

int main(int argc, char *argv[])
{
char *one 	= argv[1]; //i1
char *two 	= argv[2]; //i2
char *three 	= argv[3]; //i3
char *four 	= argv[4]; //i4

char *e;		   //o0
char *f;		   //o1

e = or(
	((and(and(and(not(one),two)),(not(three)),not(four)))),
	(and(and(and(one,not(two)),not(three)),not(four)))
      );

f = or(
(
    and(
	(not(four)),
	(and(three,(
		    and()
		   )
	    ),)
	)
)
,
);


fprintf(stdout, "\nA B C D | E F\n");
fprintf(stdout, "---------------\n");
fprintf(stdout, "%s %s %s %s | %s %s\n",one,two,three,four,e,f); 
}

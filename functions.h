#include <stdio.h>
#include <stdlib.h>

//global variables
char p = '1';
char o = '0';

//AND: result is 1 if (BOTH) input == 1
char* and(char *a, char *b)
{
    char* result = (char*)malloc(sizeof(char));

	if (*a == p)
	{
		if (*b == p)
		{
		    (*result) = p;
		    return (result);
		}
		else
		    (*result) = o;
		    return (result);
	}
	else
		(*result) = o;
		return (result);
}

//OR: result is 1 if (ONE) input == 1
char* or(char *a, char *b)
{
    char* result = (char*)malloc(sizeof(char));

	if (*a == p)
	{
	    (*result) = p;
	    return (result);
	}
	else if (*b == p)
	{
	    (*result) = p;
	    return (result);
	}
	else
	{
	    (*result) = o;
	    return (result);
	}

}

//NOT: result is inverted input
char* not(char *a)
{
    char* result = (char*)malloc(sizeof(char));

	if (*a == o)
	{
	    (*result) = p;
	    return (result);
	}
	else
	    (*result) = o;
	    return (result);
}

//XOR: result is 1 if (ONLY ONE) input == 1
char* xor(char *a, char *b)
{
	return or(and(not(a), b), and(a, not(b)));
}

//NAND: result is 1 if (ONE) input == 0 
char* nand(char *a, char *b)
{
	return (not(and(a, b)));
}

//NOR: result is 1 if (BOTH) input == 0
char* nor(char *a, char *b)
{
	return (not(or(a, b)));
}

//XNOR: result is 1 if (BOTH) input is the same value
char* xnor(char *a, char *b)
{
	return (not(xor(a, b)));
}
